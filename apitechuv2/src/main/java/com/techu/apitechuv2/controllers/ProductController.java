package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.ProductService;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2") //indicamos la ruta base para el controlador
public class ProductController {

    @Autowired
    ProductService productService;


    @GetMapping("/products")
    public List<ProductModel> getProducts() {
        System.out.println("getProducts");

        return this.productService.findAll();
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println(("La id del producto a crear es: "+product.getId()));
        System.out.println(("La desc del producto a crear es: "+product.getDesc()));
        System.out.println(("El precio del producto a crear es: "+product.getPrice()));

        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println ("La id del producto a buscar es: "+id);

        Optional<ProductModel> result = this.productService.findById(id);
        /*
        if (result.isPresent() == true){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }

        */

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct (@RequestBody ProductModel product, @PathVariable String id){

        System.out.println("updateProduct");
        System.out.println("El id de producto a actualizar: " + id);
        System.out.println("La descripcion de nuevo producto es: " + product.getDesc());
        System.out.println("El precio de nuevo producto es: " + product.getPrice());

        ResponseEntity response = null;

        //buscar por id
        Optional<ProductModel> sProduct = this.productService.findById(id);

        if (sProduct.isPresent()){
            System.out.println("Producto encontrado");
            //actualizar producto
            product.setId(id);
            product.setDesc(sProduct.get().getDesc());
            product.setPrice(sProduct.get().getPrice());
            //añadimos para actualizar
            this.productService.add(product);
            System.out.println("Producto actualizado");
            response = new ResponseEntity(product,HttpStatus.OK);
        }else {
            System.out.println("Producto no encontrado");
            response = new ResponseEntity("Producto no encontrado",HttpStatus.NOT_FOUND);
        }

        return response;
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("El id de producto a borrar es: " + id);

        ResponseEntity response = null;

        Optional<ProductModel> sProduct = this.productService.findById(id);

        if (sProduct.isPresent()) {
            System.out.println("Producto encontrado");
            this.productService.delete(sProduct.get());
            System.out.println("Producto borrado");
            response = new ResponseEntity("Producto borrado.", HttpStatus.OK);
        } else {
            System.out.println("Producto no encontrado");
            response = new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        }

        return response;
    }

}
