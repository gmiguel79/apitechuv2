package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.ProductService;
import com.techu.apitechuv2.services.UserService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2") //indicamos la ruta base para el controlador
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam (name = "$orderBy", required = false) String orderBy) {
        System.out.println("getUsers");
        System.out.println("query param: " + orderBy);

        return new ResponseEntity<>(this.userService.getUsers(orderBy), HttpStatus.OK);

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println(("La id del usuario a crear es: "+user.getId()));
        System.out.println(("El nombre del usuario a crear es: "+user.getName()));
        System.out.println(("La edad del usuario a crear es: "+user.getAge()));

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println ("La id del usuario a buscar es: "+id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser (@RequestBody UserModel user, @PathVariable String id){

        System.out.println("updateUser");
        System.out.println("El id de usuario a actualizar: " + id);
        System.out.println("La descripcion de nuevo usuario es: " + user.getName());
        System.out.println("El precio de nuevo usuario es: " + user.getAge());

        ResponseEntity response = null;

        //buscar por id
        Optional<UserModel> sUser = this.userService.findById(id);

        if (sUser.isPresent()){
            System.out.println("Usuario encontrado");
            //actualizar usuario
            user.setId(id);
            user.setName(sUser.get().getName());
            user.setAge(sUser.get().getAge());
            //añadimos para actualizar
            this.userService.add(user);
            System.out.println("Usuario actualizado");
            response = new ResponseEntity(user,HttpStatus.OK);
        }else {
            System.out.println("Usuario no encontrado");
            response = new ResponseEntity("Usuario no encontrado",HttpStatus.NOT_FOUND);
        }

        return response;
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("El id de usuario a borrar es: " + id);

        ResponseEntity response = null;

        Optional<UserModel> sUser = this.userService.findById(id);

        if (sUser.isPresent()) {
            System.out.println("Usuario encontrado");
            this.userService.delete(sUser.get());
            System.out.println("Usuario borrado");
            response = new ResponseEntity("Usuario borrado.", HttpStatus.OK);
        } else {
            System.out.println("Usuario no encontrado");
            response = new ResponseEntity("Usuario no encontrado", HttpStatus.NOT_FOUND);
        }

        return response;
    }
}
