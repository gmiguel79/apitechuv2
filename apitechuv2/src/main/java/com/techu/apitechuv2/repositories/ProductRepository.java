package com.techu.apitechuv2.repositories;

import com.techu.apitechuv2.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/*
Patrón de diseño. Realiza las consultas a la bbdd. Tiene que estar anotada con REpository y extender a MongoRepository<Tipo del modelo, Tipo de la clave>
 */
@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String> {


}

