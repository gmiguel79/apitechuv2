package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll");
        return this.userRepository.findAll();
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById");
        System.out.println("Obteniendo el usuario con la id: "+id);

        return this.userRepository.findById(id);
    }

   public List<UserModel> getUsers(String orderBy){
        System.out.println("findAllOrderBy");
        List<UserModel> result;
        if(orderBy != null) {
            System.out.println("se ha pedido ordenación");
            result = this.userRepository.findAll(Sort.by("age"));
        } else {
            result = this.userRepository.findAll();
        }
        return result;
    }

    public UserModel add(UserModel user){
        System.out.println("add");

        return this.userRepository.save(user);
    }

    public void delete(UserModel user) {
        System.out.println("delete");
        this.userRepository.delete(user);
    }
}
